import { players as playersData } from '../data';

export const players = (state = playersData, action) => {
  switch (action.type) {
  case 'SELECT_PLAYER':
    return state.map((player, index) => {
      if (index === action.index) {
        return {
          ...player,
          selected: action.selected
        };
      }
      return player;
    });
  case 'SET_PLAYER_STATUSES':
    return state.map((player, index) => {
      return player;
    });

  default:
    return state;
  }
};
