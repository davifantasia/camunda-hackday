import { combineReducers } from 'redux';
import { players } from './reducers';

export default combineReducers({
  players
});
