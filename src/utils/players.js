export const askPlayers = () => {
  return new Promise(resolve => {
    resolve([
      { index: 0, status: 'pending' },
      { index: 1, status: 'accepted' },
      { index: 2, status: 'accepted' },
      { index: 3, status: 'rejected' }
    ]);
  });
};
