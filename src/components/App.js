import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Table, Checkbox, Button } from 'semantic-ui-react';
import { selectPlayer, setPlayersStatuses } from '../action-creators';
import styled from 'styled-components';
import { askPlayers } from '../utils/players';

const Container = styled.div`
  text-align: center;
`;

const AppHeader = styled.header`
  background-color: #282c34;
  padding: 30px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
`;

class App extends Component {
  render() {
    return (
      <Container>
        <AppHeader>
          <h1>Darts Game for Camunda Hackday</h1>
        </AppHeader>

        <section>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Players</Table.HeaderCell>
                <Table.HeaderCell>Select</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {Object.keys(this.props.players).map(key => this.renderRow(key))}
            </Table.Body>
          </Table>

          <Button onClick={this.askPlayers}>ASK PLAYERS</Button>

          <pre>{JSON.stringify(this.props)}</pre>
        </section>
      </Container>
    );
  }

  renderRow = key => {
    const players = this.props.players;
    return (
      <Table.Row key={key}>
        <Table.Cell>{players[key].name}</Table.Cell>
        <Table.Cell>
          <Checkbox toggle onChange={this.onPlayerSelectToggle} value={key} />
        </Table.Cell>
        <Table.Cell>{players[key].status}</Table.Cell>
      </Table.Row>
    );
  };

  onPlayerSelectToggle = (event, v) => {
    var { value, checked } = v;
    this.props.selectPlayer(+value, checked);
  };

  askPlayers = () => {
    askPlayers(this.props.players).then(status => {
      console.log(status);
    });
  };
}

const mapStateToProps = state => ({
  players: state.players
});

App.propTypes = {
  players: PropTypes.array,
  selectPlayer: PropTypes.func,
  setPlayersStatuses: PropTypes.func
};

export default connect(
  mapStateToProps,
  { selectPlayer }
)(App);
