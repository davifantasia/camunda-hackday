export const selectPlayer = (index, selected) => dispatch => {
  dispatch({
    type: 'SELECT_PLAYER',
    index,
    selected
  });
};

export const setPlayersStatuses = statuses => dispatch => {
  dispatch({
    type: 'SET_PLAYERS_STATUSES',
    statuses
  });
};
