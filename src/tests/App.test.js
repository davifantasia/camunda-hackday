import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../components/App';

Enzyme.configure({ adapter: new Adapter() });

let wrapper = shallow(
  <App />
);

it('renders without crashing', () => {
  expect(wrapper.length).toEqual(1);
});
